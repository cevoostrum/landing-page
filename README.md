# Instructions

First install the global dependencies:

```
npm run prepare
```

Afterwards you can either render once:

```
npm run render
```

or watch for changes:

```
npm run watch
```
