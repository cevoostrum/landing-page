_OUTPUT_DIR=dist
_TEMP_FILE=$_OUTPUT_DIR/index-raw.html
_OUTPUT_FILE=$_OUTPUT_DIR/index.html
echo $(lumo -c deps/reagent-0.7.0.jar:deps/react-15.6.2-0.jar:deps/react-dom-15.6.2-0.jar:deps/js-yaml-3.3.1-0.jar:deps/react-dom-server-15.6.2-0.jar:deps/create-react-class-15.6.2-0.jar:deps/ui-components-0.1.15-SNAPSHOT.jar:deps/cuerdas-2.0.3.jar:deps/shared-0.11.8.jar:deps/semantic-ui-react-0.71.0-0.jar:src render.cljs) > $_TEMP_FILE
js-beautify $_TEMP_FILE > $_OUTPUT_FILE
rm $_TEMP_FILE
