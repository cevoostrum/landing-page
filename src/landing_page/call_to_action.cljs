(ns landing-page.call-to-action
  (:require [landing-page.section :refer [Section]]
            [landing-page.sign-up-form :refer [SignUpForm]]))

(defn CallToAction [{:keys [section-type content]}]
  [Section {:type section-type
            :padded true}
   [:div.ui.text.container
    [:h1.ui.header content]
    [SignUpForm]]])
