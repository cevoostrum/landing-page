(ns landing-page.footer
  (:require [clojure.string :as str]
            [landing-page.section :refer [Section]]))

#_:div.ui.black.inverted.vertical.section.center.aligned.segment

(defn Footer [{:keys [section-type name street postal social-providers]}]
  [Section {:type section-type
            :inverted true}
   [:div.ui.container.left.aligned
    [:img.footer-logo.center.aligned {:src "/images/logo.svg" :alt "offcourse logo"}]
    [:div.ui.two.column.left.aligned.stackable.grid
     [:div.ui.column
      [:p.name name]
      [:p street]
      [:p postal]]
     [:div.ui.column.right.aligned
      (for [{:keys [name link]} social-providers]
        ^{:key name} [:a {:href link} name])]]]])
