(ns landing-page.logo)

(defn Logo []
  [:div.ui.inverted.logo
   [:img {:src "/images/logo.svg" :alt "offcourse logo"}]])
