(ns landing-page.section
  (:require [clojure.string :as str]))

(defn classes [classes]
  (->> classes
       flatten
       (str/join " ")))

(defn Section [{:keys [type color inverted padded size]} & children]
  [:section.row
   {:class (classes [type "section"])}
   [:div.ui.vertical.center.aligned.segment
    {:class (when inverted (classes [(or color "black") "inverted"]))}
    (map-indexed #(with-meta %2 {:key %1}) children)]])
