(ns landing-page.step
  (:require [cuerdas.core :as str]))

(defn Step [{:keys [title icon content url]}]
  [:div.ui.column {:href (str "#" title) :id (str title) :class "step"}
   [:div.ui.padded.segment.usp
    [:img.icon {:src url :alt "icon"}]
    [:h1.ui.header (str/title title)]
    [:p content]]])