(ns landing-page.template
  (:require [landing-page.head :refer [Head]]
            [landing-page.topbar :refer [Topbar]]
            [landing-page.footer :refer [Footer]]
            [landing-page.call-to-action :refer [CallToAction]]
            [landing-page.splash :refer [Splash]]
            [landing-page.steps :refer [Steps]]
            [landing-page.error :refer [Error]]))

(defn template [page-content]
  [:html
   [Head]
   [:body
    [:div#app
     [Topbar]
     [:ui.grid
      (for [{:keys [section-type] :as section-data} page-content]
        (case section-type
          "call-to-action" ^{:key section-type}[CallToAction section-data]
          "topbar"         ^{:key section-type}[Topbar section-data]
          "splash"         ^{:key section-type}[Splash section-data]
          "steps"          ^{:key section-type}[Steps section-data]
          "footer"         ^{:key section-type}[Footer section-data]
          ^{:key section-type}[Error]))]]]])

