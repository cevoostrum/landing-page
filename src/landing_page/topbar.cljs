(ns landing-page.topbar
  (:require [landing-page.steps :refer [Steps]]
            [landing-page.logo :refer [Logo]]
            [landing-page.sign-up-form :refer [SignUpForm]]
            [landing-page.section :refer [Section]]))

(defn Topbar []
  [Section {:type "topbar"
            :size :large
            :padded true}
   [:div.ui.container
    [:div.ui.stackable.grid
     [:div.ui.column
      [Logo]]]]])
